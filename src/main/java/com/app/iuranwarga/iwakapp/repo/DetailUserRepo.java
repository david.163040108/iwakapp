package com.app.iuranwarga.iwakapp.repo;

import com.app.iuranwarga.iwakapp.entity.DetailUser;
import com.app.iuranwarga.iwakapp.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DetailUserRepo extends PagingAndSortingRepository<DetailUser, Long> {

    Page<DetailUser> findAllByidRt(String idRt, Pageable pageable);
    Page<DetailUser> findAllByuserRt(User userRt,Pageable pageable);
    DetailUser findByUser(User user);

}
