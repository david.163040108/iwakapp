package com.app.iuranwarga.iwakapp.repo;

import com.app.iuranwarga.iwakapp.entity.IuranPerbulan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IuranPerbulanRepoJpa extends JpaRepository<IuranPerbulan, Long> {

}
