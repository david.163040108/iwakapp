package com.app.iuranwarga.iwakapp.repo;

import com.app.iuranwarga.iwakapp.entity.Pengeluaran;
import com.app.iuranwarga.iwakapp.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PengeluaranRepo extends PagingAndSortingRepository<Pengeluaran, Integer> {

    Page<Pengeluaran> findAllByUser(User user,Pageable pageable);
}
