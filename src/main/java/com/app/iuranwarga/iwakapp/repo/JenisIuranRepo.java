package com.app.iuranwarga.iwakapp.repo;

import com.app.iuranwarga.iwakapp.entity.JenisIuran;
import com.app.iuranwarga.iwakapp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JenisIuranRepo extends JpaRepository<JenisIuran, Integer> {
    public List<JenisIuran> findByUser(User user);
}
