package com.app.iuranwarga.iwakapp.repo;

import com.app.iuranwarga.iwakapp.entity.Broadcast;
import com.app.iuranwarga.iwakapp.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BroadCastRepo extends JpaRepository<Broadcast,Long> {
    Page<Broadcast> findAllByUser(User user , Pageable pageable);
}
