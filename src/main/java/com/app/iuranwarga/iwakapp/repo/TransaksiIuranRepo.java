package com.app.iuranwarga.iwakapp.repo;

import com.app.iuranwarga.iwakapp.entity.DetailUser;
import com.app.iuranwarga.iwakapp.entity.TransaksiIuran;
import com.app.iuranwarga.iwakapp.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransaksiIuranRepo extends PagingAndSortingRepository<TransaksiIuran, Long> {

    Page<TransaksiIuran> findAllByidRt(String idRt, Pageable pageable);
    Page<TransaksiIuran> findAllByUser(User user, Pageable pageable);
    Page<TransaksiIuran> findAllByUserAndActive(User user,boolean active ,Pageable pageable);
}
