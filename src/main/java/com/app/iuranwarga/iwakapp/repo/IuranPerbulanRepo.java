package com.app.iuranwarga.iwakapp.repo;

import com.app.iuranwarga.iwakapp.entity.IuranPerbulan;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IuranPerbulanRepo extends PagingAndSortingRepository<IuranPerbulan, Long> {
}
