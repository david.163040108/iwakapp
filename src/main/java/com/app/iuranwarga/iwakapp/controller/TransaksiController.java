package com.app.iuranwarga.iwakapp.controller;

import com.app.iuranwarga.iwakapp.entity.DetailUser;
import com.app.iuranwarga.iwakapp.entity.TransaksiIuran;
import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.service.TransaksiIuranService;
import com.app.iuranwarga.iwakapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/transaksi")
public class TransaksiController {

    @Autowired
    private TransaksiIuranService iuranService;
    @Autowired
    private UserService userService;

    @GetMapping("/showTransaksi")
    public String transaksiWarga(Model theModel, Pageable pageable) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());
        String converId = String.valueOf(user.getId());

        Page<TransaksiIuran> transaksiIurans = iuranService.transaksiByRt(converId, pageable);
        theModel.addAttribute("listTransaksi", transaksiIurans);
        return "transaksi/transaksi-list";
    }
}
