package com.app.iuranwarga.iwakapp.controller.warga;

import com.app.iuranwarga.iwakapp.entity.TransaksiIuran;
import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.service.TransaksiIuranService;
import com.app.iuranwarga.iwakapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/tagihan")
public class TransaksiWargaController {

    @Autowired
    private TransaksiIuranService iuranService;
    @Autowired
    private UserService userService;

    @GetMapping("/showTagihan")
    public String tagihanWarga(Model theModel, Pageable pageable) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());

//        Page<TransaksiIuran> transaksiIurans = iuranService.findByUser(user,pageable);
//        Page<TransaksiIuran> thtrue = iuranService.findAllByUserAndActive(user,true,pageable);

        Page<TransaksiIuran> thfalse = iuranService.findAllByUserAndActive(user,false,pageable);
        theModel.addAttribute("listTransaksi", thfalse);

        return "tagihan/tagihan-list";
    }

    @GetMapping("/showRiwayatTagihan")
    public String riwayatTagihanWarga(Model theModel, Pageable pageable) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());

//        Page<TransaksiIuran> transaksiIurans = iuranService.findByUser(user,pageable);
        Page<TransaksiIuran> thtrue = iuranService.findAllByUserAndActive(user,true,pageable);

//        Page<TransaksiIuran> thfalse = iuranService.findAllByUserAndActive(user,false,pageable);
        theModel.addAttribute("listTransaksi", thtrue);

        return "tagihan/riwayat-tagihan";
    }
}
