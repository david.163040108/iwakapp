package com.app.iuranwarga.iwakapp.controller;

import com.app.iuranwarga.iwakapp.entity.DetailUser;
import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.service.DetailUserService;
import com.app.iuranwarga.iwakapp.service.UserService;
import com.app.iuranwarga.iwakapp.service.impl.DetailUserServiceImpl;
import com.app.iuranwarga.iwakapp.user.CrmUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/profile")
public class ProfilController {
    @Autowired
    private DetailUserService detailUserService;

    @Autowired
    private UserService userService;


    @GetMapping("/")
    public String showMyLoginPage(Model theModel) {
        //Get User
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails =(UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());
        //get Detail User
        DetailUser detailUser = detailUserService.findByUser(user);

        theModel.addAttribute("detailUser", detailUser);
        theModel.addAttribute("user", user);
        return "profile/profile";
    }

    @GetMapping("/rt")
    public String testData(Model theModel, Pageable pageable) {
        String rt = "1";
        Page<DetailUser> detailUsers = detailUserService.detilUserByRT(rt, pageable);
        for ( DetailUser m : detailUsers) {
            System.out.println( m.getUser().getUserName());
        }
        return "profile/profile";
    }
}
