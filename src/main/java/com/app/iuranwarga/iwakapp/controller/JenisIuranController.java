package com.app.iuranwarga.iwakapp.controller;

import com.app.iuranwarga.iwakapp.entity.Broadcast;
import com.app.iuranwarga.iwakapp.entity.IuranPerbulan;
import com.app.iuranwarga.iwakapp.entity.JenisIuran;
import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.service.UserService;
import com.app.iuranwarga.iwakapp.service.impl.JenisIuranService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/jenis")
public class JenisIuranController {
    @Autowired
    private JenisIuranService iuranService;
    @Autowired
    private UserService userService;

    @GetMapping("/showIuran")
    public String showBroadcast(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());

        List<JenisIuran> list = iuranService.getByRt(user);
        model.addAttribute("iuran", list);
        return "jenis-iuran/jenis-iuran";
    }

    @GetMapping("/showFormForAdd")
    public String showFormForAdd(Model theModel) {

        // create model attribute to bind form data
        JenisIuran jenisIuran = new JenisIuran();

        theModel.addAttribute("jenis", jenisIuran);

        return "/jenis-iuran/jenisiuran-form";
    }

    @GetMapping("/showFormForUpdate")
    public String showFormForUpdate(@RequestParam("jenisId") int theId,
                                    Model theModel) {

        // get the employee from the service
        JenisIuran jenisIuran = iuranService.findById(theId);

        // set employee as a model attribute to pre-populate the form
        theModel.addAttribute("jenis", jenisIuran);

        // send over to our form
        return "/jenis-iuran/jenisiuran-form";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam("jenisId") int theId) {

        // delete the employee
        iuranService.delete(theId);
        System.out.println("ini id" + theId);
        return "redirect:/jenis/showIuran";

    }

    @PostMapping("/save")
    public String saveEmployee(@ModelAttribute("jenis") JenisIuran js, Authentication authentication) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());
        JenisIuran jenisIuran = new JenisIuran();
        jenisIuran.setId(js.getId());
        jenisIuran.setNamaIuran(js.getNamaIuran());
        jenisIuran.setNominal(js.getNominal());
        jenisIuran.setUser(user);
        iuranService.save(jenisIuran);
        // use a redirect to prevent duplicate submissions
        return "redirect:/jenis/showIuran";
    }

}
