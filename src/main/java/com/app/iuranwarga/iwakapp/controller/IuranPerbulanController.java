package com.app.iuranwarga.iwakapp.controller;

import com.app.iuranwarga.iwakapp.entity.*;
import com.app.iuranwarga.iwakapp.repo.IuranPerbulanRepo;
import com.app.iuranwarga.iwakapp.repo.IuranPerbulanRepoJpa;
import com.app.iuranwarga.iwakapp.service.DetailUserService;
import com.app.iuranwarga.iwakapp.service.TransaksiIuranService;
import com.app.iuranwarga.iwakapp.service.UserService;
import com.app.iuranwarga.iwakapp.service.impl.IuranPerbulanService;
import com.app.iuranwarga.iwakapp.service.impl.JenisIuranService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/iuran")
public class IuranPerbulanController {

    @Autowired
    private IuranPerbulanRepo iuranPerbulanRepo;
    @Autowired
    private IuranPerbulanRepoJpa repoJpa;

    @Autowired
    private IuranPerbulanService service;
    @Autowired
    private UserService userService;
    @Autowired
    private JenisIuranService iuranService;

    @Autowired
    private DetailUserService detailUserService;
    @Autowired
    private TransaksiIuranService transaksiIuranService;

    @GetMapping("/showIuranPerbulan")
    public String showBroadcast(Model model, Pageable pageable) {
        Page<IuranPerbulan> list = iuranPerbulanRepo.findAll(pageable);
        model.addAttribute("iuran", list);
        return "iuran-perbulan/iuran-list";
    }

    @GetMapping("/showFormForAdd")
    public String showFormForAdd(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());

        List<JenisIuran> list = iuranService.getByRt(user);
        for (JenisIuran m : list) {
            System.out.println(m.getNamaIuran());
        }
        model.addAttribute("list", list);

        IuranPerbulan perbulan = new IuranPerbulan();

        model.addAttribute("perbulan", perbulan);

        return "/iuran-perbulan/iuran-form";
    }

    @GetMapping("/showFormForUpdate")
    public String showFormForUpdate(@RequestParam("iuranId") int theId,
                                    Model theModel) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());

        IuranPerbulan perbulan1 = service.findById(theId);
        System.out.println(perbulan1.getTanggalIuran());
        System.out.println(perbulan1.getJenisIurans());
        IuranPerbulan perbulan = new IuranPerbulan();
        List<JenisIuran> list = iuranService.getByRt(user);
        theModel.addAttribute("list", list);

        theModel.addAttribute("perbulan", perbulan1);

        return "/iuran-perbulan/iuran-form";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute("perbulan") IuranPerbulan ib) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());
        IuranPerbulan perbulan = new IuranPerbulan();
        perbulan.setId(ib.getId());
        perbulan.setUser(user);
        perbulan.setTanggalIuran(ib.getTanggalIuran());
        perbulan.setJenisIurans(ib.getJenisIurans());
        perbulan.setActive(false);
        iuranPerbulanRepo.save(perbulan);
        // use a redirect to prevent duplicate submissions
        return "redirect:/iuran/showIuranPerbulan";
    }

    @GetMapping("/generate")
    public String generateTransaksi(@RequestParam("iuranId") int theId,
                                    Model theModel, Pageable pageable) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());
        String converId = String.valueOf(user.getId());
        Page<DetailUser> detailUsers = detailUserService.detilUserByRT(converId,pageable);

        IuranPerbulan perbulan1 = service.findById(theId);
        IuranPerbulan updateIuran = new IuranPerbulan();
        updateIuran = perbulan1;
        updateIuran.setActive(true);
        for (DetailUser d : detailUsers) {
            TransaksiIuran iuran = new TransaksiIuran();
            iuran.setTanggalIuran(perbulan1.getTanggalIuran());
            iuran.setActive(false);
            iuran.setUser(d.getUser());
            iuran.setIdRt(converId);
            iuran.setIuranPerbulan(perbulan1);
            transaksiIuranService.save(iuran);
        }
        iuranPerbulanRepo.save(updateIuran);

        return "redirect:/iuran/showIuranPerbulan";
    }
}
