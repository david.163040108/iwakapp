package com.app.iuranwarga.iwakapp.controller.warga;

import com.app.iuranwarga.iwakapp.entity.Broadcast;
import com.app.iuranwarga.iwakapp.entity.DetailUser;
import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.service.UserService;
import com.app.iuranwarga.iwakapp.service.impl.BroadCastService;
import com.app.iuranwarga.iwakapp.service.impl.DetailUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/info")
public class InformasiController {
    @Autowired
    private BroadCastService broadCastService;
    @Autowired
    private UserService userService;
    @Autowired
    private DetailUserServiceImpl detailUserService;

    @GetMapping("/showInfo")
    public String showInfo(Model model, Pageable pageable){
        //Get User
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails =(UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());
        //get Detail User
        DetailUser detailUser = detailUserService.findByUser(user);
        //Untuk Get User RT
        User rt = new User();
        rt.setId(Long.parseLong(detailUser.getIdRt()));
        Page<Broadcast> list = broadCastService.findByUser(rt, pageable);

        model.addAttribute("listInfo", list);

        return "informasi/info-list";
    }
}
