package com.app.iuranwarga.iwakapp.controller;

import com.app.iuranwarga.iwakapp.entity.DetailUser;
import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.service.DetailUserService;
import com.app.iuranwarga.iwakapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/warga")
public class DaftarWargaController {
    @Autowired
    private DetailUserService detailUserService;
    @Autowired
    private UserService userService;

    @GetMapping("/showWarga")
    public String showMyLoginPage(Model theModel, Pageable pageable) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails =(UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());
        String converId = String.valueOf(user.getId());
        Page<DetailUser> detailUsers = detailUserService.findAllByuserRt(user,pageable);

        theModel.addAttribute("listWarga", detailUsers);

        return "daftar-warga/warga-list";
    }

    @GetMapping("/rt")
    public String daftarWarga(Model theModel, Pageable pageable) {
        String rt = "1";
        Page<DetailUser> detailUsers = detailUserService.detilUserByRT(rt, pageable);
        for ( DetailUser m : detailUsers) {
            System.out.println( m.getUser().getUserName());
        }
        return "profile/profile";
    }
    @GetMapping("/save")
    public String verifikasiTrue(@RequestParam("wargaId") int theId , @RequestParam("status") boolean status) {

        // delete the employee
        DetailUser detailUser = detailUserService.findById(theId);
        DetailUser newDetail = new DetailUser();
        newDetail.setId(detailUser.getId());
        newDetail.setNoRumah(detailUser.getNoRumah());
        newDetail.setNoKK(detailUser.getNoKK());
        newDetail.setUser(detailUser.getUser());
        newDetail.setJmlAnggotaKeluarga(detailUser.getJmlAnggotaKeluarga());
        newDetail.setNoHp(detailUser.getNoHp());
        newDetail.setIdRt(detailUser.getIdRt());
        if (status){
            newDetail.setActive(false);
        }else {
            newDetail.setActive(true);
        }

        detailUserService.save(newDetail);
        return "redirect:/warga/showWarga";

    }
}
