package com.app.iuranwarga.iwakapp.controller;

import com.app.iuranwarga.iwakapp.entity.Broadcast;
import com.app.iuranwarga.iwakapp.entity.Pengeluaran;
import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.repo.PengeluaranRepo;
import com.app.iuranwarga.iwakapp.service.UserService;
import groovy.util.logging.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
@Slf4j
@Controller
@RequestMapping("/pengeluaran")
public class PengeluaranController {
    @Autowired
    private PengeluaranRepo pengeluaranRepo;

    @Autowired
    private UserService userService;

    @GetMapping("/showPengeluaran")
    public String showBroadcast(Model model, Pageable pageable ,Authentication authentication){
        User user = userService.findByUserName(authentication.getName());

        Page<Pengeluaran> page = pengeluaranRepo.findAllByUser(user,pageable);
        model.addAttribute("pengeluaran",  page);
        model.addAttribute("pln",  "pengeluaran");
        return "pengeluaran/pengeluaran-list";
    }
}
