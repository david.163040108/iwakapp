package com.app.iuranwarga.iwakapp.controller;

import com.app.iuranwarga.iwakapp.dao.impl.BroadcastDaoImpl;
import com.app.iuranwarga.iwakapp.entity.Broadcast;
import com.app.iuranwarga.iwakapp.entity.JenisIuran;
import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.service.UserService;
import com.app.iuranwarga.iwakapp.service.impl.BroadCastService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/broadcast")
public class BroadCastController {

    @Autowired
    private BroadCastService broadCastService;
    @Autowired
    private UserService userService;
    @GetMapping("/showBroadcast")
    public String showBroadcast(Model model, Authentication authentication){
        List<Broadcast> list = broadCastService.findAll();
        model.addAttribute("bc", list);
        model.addAttribute("user", authentication.getName());
        return "broadcast/broadcast";
    }

    @GetMapping("/showFormForAdd")
    public String showFormForAdd(Model theModel) {

        // create model attribute to bind form data
        Broadcast theBroadcast = new Broadcast();

        theModel.addAttribute("broadcast", theBroadcast);

        return "/broadcast/broadcast-form";
    }

    @PostMapping("/save")
    public String saveEmployee(@ModelAttribute("employee") Broadcast bc, Authentication authentication) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails =(UserDetails) auth.getPrincipal();
        User user = userService.findByUserName(userDetails.getUsername());
        LocalDate date = LocalDate.now();
        Broadcast broadcast = new Broadcast();
        broadcast.setId(bc.getId());
        broadcast.setInformasi(bc.getInformasi());
        broadcast.setLink(bc.getLink());
        broadcast.setTanggalPost(date);
        broadcast.setUser(user);
        broadCastService.save(broadcast);
        // use a redirect to prevent duplicate submissions
        return "redirect:/broadcast/showBroadcast";
    }
    @GetMapping("/showFormForUpdate")
    public String showFormForUpdate(@RequestParam("broadcastId") int theId,
                                    Model theModel) {

        // get the employee from the service
        Broadcast bc = broadCastService.findById(theId);

        // set employee as a model attribute to pre-populate the form
        theModel.addAttribute("broadcast", bc);

        // send over to our form

        return "/broadcast/broadcast-form";
    }
    @GetMapping("/delete")
    public String delete(@RequestParam("broadcastId") int theId) {

        // delete the employee
        broadCastService.delete(theId);
        System.out.println("ini id" + theId);
        return "redirect:/broadcast/showBroadcast";

    }

}
