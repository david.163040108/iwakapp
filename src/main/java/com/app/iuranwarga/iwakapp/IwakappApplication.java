package com.app.iuranwarga.iwakapp;

import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.springdata.SpringDataDialect;


@SpringBootApplication
public class IwakappApplication {

	public static void main(String[] args) {
		SpringApplication.run(IwakappApplication.class, args);
	}

	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}
	@Bean
	public SpringDataDialect springDataDialect() {
		return new SpringDataDialect();
	}

}
