package com.app.iuranwarga.iwakapp.config;

import com.app.iuranwarga.iwakapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class AuthSecurity extends WebSecurityConfigurerAdapter {

    private final String ADMIN = "ADMIN";
    private final String RT = "RT";
    private  final String WARGA ="WARGA";
    @Autowired
    private UserService userService;

    @Autowired
    private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").hasRole("WARGA")
                .antMatchers("/leaders/**").hasRole("WARGA")
                .antMatchers("/transaksi/**").hasRole(WARGA)
                .antMatchers("/broadcast/**").hasAnyRole(WARGA,RT)
                .antMatchers("/jenis/**").hasAnyRole(WARGA,RT)
                .antMatchers("/iuran/**").hasAnyRole(WARGA,RT)
                .antMatchers("/warga/**").hasAnyRole(WARGA,RT)
                .antMatchers("/pengeluaran/**").hasAnyRole(WARGA,RT)

                .antMatchers("/systems/**").hasRole("ADMIN")
                .and()
                .formLogin()
                .loginPage("/showMyLoginPage")
                .loginProcessingUrl("/authenticateTheUser")
                .successHandler(customAuthenticationSuccessHandler)
                .permitAll()
                .and()
                .logout().logoutUrl("/logout-custom").logoutSuccessUrl("/showMyLoginPage").permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/access-denied");
    }

    //beans
    //bcrypt bean definition
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    //authenticationProvider bean definition
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
        auth.setUserDetailsService(userService); //set the custom user details service
        auth.setPasswordEncoder(passwordEncoder()); //set the password encoder - bcrypt
        return auth;
    }
}
