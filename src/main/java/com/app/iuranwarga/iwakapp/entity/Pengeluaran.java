package com.app.iuranwarga.iwakapp.entity;

import javax.persistence.*;
import java.time.LocalDate;
@Entity
@Table(name = "pengeluaran")
public class Pengeluaran {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "nama_pengeluaran")
    private String namaPengeluaran;
    @Column(name = "tanggal_pengeluaran")
    private LocalDate tanggalPengeluaran;
    @Column(name = "nominal")
    private int nominal;
    @ManyToOne(cascade = {CascadeType.PERSIST,CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "user_id")
    private User user;

    public Pengeluaran() {
    }

    public Pengeluaran(String namaPengeluaran, LocalDate tanggalPengeluaran, int nominal, User user) {
        this.namaPengeluaran = namaPengeluaran;
        this.tanggalPengeluaran = tanggalPengeluaran;
        this.nominal = nominal;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNamaPengeluaran() {
        return namaPengeluaran;
    }

    public void setNamaPengeluaran(String namaPengeluaran) {
        this.namaPengeluaran = namaPengeluaran;
    }

    public LocalDate getTanggalPengeluaran() {
        return tanggalPengeluaran;
    }

    public void setTanggalPengeluaran(LocalDate tanggalPengeluaran) {
        this.tanggalPengeluaran = tanggalPengeluaran;
    }

    public int getNominal() {
        return nominal;
    }

    public void setNominal(int nominal) {
        this.nominal = nominal;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Pengeluaran{" +
                "id=" + id +
                ", namaPengeluaran='" + namaPengeluaran + '\'' +
                ", tanggalPengeluaran=" + tanggalPengeluaran +
                ", nominal=" + nominal +
                ", user=" + user +
                '}';
    }
}
