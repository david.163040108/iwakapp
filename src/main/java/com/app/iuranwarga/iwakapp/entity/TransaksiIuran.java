package com.app.iuranwarga.iwakapp.entity;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "transaksi_iuran")
public class TransaksiIuran {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id"
    )
    private Long id;
    @Column(name = "tanggal_iuran")
    private LocalDate tanggalIuran;
    @ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "id_iuran_perbulan")
    private IuranPerbulan iuranPerbulan;
    @Column(name = "tanggal_bayar")
    private LocalDate tanggalBayar;
    @Column(name = "idrt")
    private String idRt;
    @Column(name = "active")
    private Boolean active;

    public TransaksiIuran() {
    }

    public TransaksiIuran(LocalDate tanggalIuran, User user, IuranPerbulan iuranPerbulan, LocalDate tanggalBayar, String idRt, Boolean active) {
        this.tanggalIuran = tanggalIuran;
        this.user = user;
        this.iuranPerbulan = iuranPerbulan;
        this.tanggalBayar = tanggalBayar;
        this.idRt = idRt;
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTanggalIuran() {
        return tanggalIuran;
    }

    public void setTanggalIuran(LocalDate tanggalIuran) {
        this.tanggalIuran = tanggalIuran;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public IuranPerbulan getIuranPerbulan() {
        return iuranPerbulan;
    }

    public void setIuranPerbulan(IuranPerbulan iuranPerbulan) {
        this.iuranPerbulan = iuranPerbulan;
    }

    public LocalDate getTanggalBayar() {
        return tanggalBayar;
    }

    public void setTanggalBayar(LocalDate tanggalBayar) {
        this.tanggalBayar = tanggalBayar;
    }

    public String getIdRt() {
        return idRt;
    }

    public void setIdRt(String idRt) {
        this.idRt = idRt;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "TransaksiIuran{" +
                "id=" + id +
                ", tanggalIuran=" + tanggalIuran +
                ", user=" + user +
                ", iuranPerbulan=" + iuranPerbulan +
                ", tanggalBayar=" + tanggalBayar +
                ", idRt='" + idRt + '\'' +
                ", active=" + active +
                '}';
    }
}
