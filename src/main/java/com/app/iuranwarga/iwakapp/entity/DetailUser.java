package com.app.iuranwarga.iwakapp.entity;

import javax.persistence.*;
@Entity
@Table(name = "detail_user")
public class DetailUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "no_kk")
    private String noKK;
    @Column(name = "no_rumah")
    private String noRumah;
    @Column(name = "jml_anggota_keluarga")
    private String jmlAnggotaKeluarga;
    @Column(name = "no_hp")
    private String noHp;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "rt_id")
    private User userRt;
    @Column(name = "id_rt")
    private String idRt;
    @Column(name = "active")
    private Boolean active;

    public DetailUser() {
    }

    public DetailUser(String noKK, String noRumah, String jmlAnggotaKeluarga, String noHp, User user, User userRt, String idRt, Boolean active) {
        this.noKK = noKK;
        this.noRumah = noRumah;
        this.jmlAnggotaKeluarga = jmlAnggotaKeluarga;
        this.noHp = noHp;
        this.user = user;
        this.userRt = userRt;
        this.idRt = idRt;
        this.active = active;
    }

    public DetailUser(String noKK, String noRumah, String jmlAnggotaKeluarga, String noHp, User user, String idRt, Boolean active) {
        this.noKK = noKK;
        this.noRumah = noRumah;
        this.jmlAnggotaKeluarga = jmlAnggotaKeluarga;
        this.noHp = noHp;
        this.user = user;
        this.idRt = idRt;
        this.active = active;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNoKK() {
        return noKK;
    }

    public void setNoKK(String noKK) {
        this.noKK = noKK;
    }

    public String getNoRumah() {
        return noRumah;
    }

    public void setNoRumah(String noRumah) {
        this.noRumah = noRumah;
    }

    public String getJmlAnggotaKeluarga() {
        return jmlAnggotaKeluarga;
    }

    public void setJmlAnggotaKeluarga(String jmlAnggotaKeluarga) {
        this.jmlAnggotaKeluarga = jmlAnggotaKeluarga;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getIdRt() {
        return idRt;
    }

    public void setIdRt(String idRt) {
        this.idRt = idRt;
    }

    public User getUserRt() {
        return userRt;
    }

    public void setUserRt(User userRt) {
        this.userRt = userRt;
    }

    @Override
    public String toString() {
        return "DetailUser{" +
                "id=" + id +
                ", noKK='" + noKK + '\'' +
                ", noRumah='" + noRumah + '\'' +
                ", jmlAnggotaKeluarga='" + jmlAnggotaKeluarga + '\'' +
                ", noHp='" + noHp + '\'' +
                ", user=" + user +
                ", idRt='" + idRt + '\'' +
                '}';
    }
}
