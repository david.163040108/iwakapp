package com.app.iuranwarga.iwakapp.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "iuran_perbulan")
public class IuranPerbulan {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "tanggal_iuran")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggalIuran;
    @ManyToOne(fetch = FetchType.LAZY, cascade= {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToMany(fetch = FetchType.LAZY,cascade= {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(name = "jenis_iuran_perbulan",
            joinColumns = @JoinColumn(name = "id_iuran_perbulan"),
            inverseJoinColumns = @JoinColumn(name = "id_jenis_iuran"))
    private Set<JenisIuran> jenisIurans = new HashSet<>();
    @Column(name = "active")
    private boolean active;

    public IuranPerbulan() {
    }

    public IuranPerbulan(LocalDate tanggalIuran, User user, Set<JenisIuran> jenisIurans) {
        this.tanggalIuran = tanggalIuran;
        this.user = user;
        this.jenisIurans = jenisIurans;
    }

    public IuranPerbulan(LocalDate tanggalIuran, User user, boolean active) {
        this.tanggalIuran = tanggalIuran;
        this.user = user;
        this.active = active;
    }

    public IuranPerbulan(LocalDate tanggalIuran, User user, Set<JenisIuran> jenisIurans, boolean active) {
        this.tanggalIuran = tanggalIuran;
        this.user = user;
        this.jenisIurans = jenisIurans;
        this.active = active;
    }

    public IuranPerbulan(LocalDate tanggalIuran, User user) {
        this.tanggalIuran = tanggalIuran;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTanggalIuran() {
        return tanggalIuran;
    }

    public void setTanggalIuran(LocalDate tanggalIuran) {
        this.tanggalIuran = tanggalIuran;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<JenisIuran> getJenisIurans() {
        return jenisIurans;
    }

    public void setJenisIurans(Set<JenisIuran> jenisIurans) {
        this.jenisIurans = jenisIurans;
    }

    public void addJenisIuran(JenisIuran jenisIuran) {
        this.jenisIurans.add(jenisIuran);
        jenisIuran.getIuranPerbulans().add(this);
    }

    public void removeJenisIuran(JenisIuran jenisIuran) {
        this.jenisIurans.remove(jenisIuran);
        jenisIuran.getIuranPerbulans().remove(this);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "IuranPerbulan{" +
                "id=" + id +
                ", tanggalIuran=" + tanggalIuran +
                ", jenisIurans=" + jenisIurans +
                '}';
    }
}
