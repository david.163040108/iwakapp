package com.app.iuranwarga.iwakapp.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "broadcast")
public class Broadcast {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "informasi")
    private String informasi;
    @Column(name = "link")
    private String link;
    @Column(name = "tanggalPost")
    private LocalDate tanggalPost;
    @ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    public Broadcast(){

    }

    public Broadcast(String informasi, String link, LocalDate tanggalPost, User user) {
        this.informasi = informasi;
        this.link = link;
        this.tanggalPost = tanggalPost;
        this.user = user;
    }

    public Broadcast(Long id, String informasi, String link, User user) {
        this.id = id;
        this.informasi = informasi;
        this.link = link;
        this.user = user;
    }

    public Broadcast(String informasi, String link, User user) {
        this.informasi = informasi;
        this.link = link;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInformasi() {
        return informasi;
    }

    public void setInformasi(String informasi) {
        this.informasi = informasi;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getTanggalPost() {
        return tanggalPost;
    }

    public void setTanggalPost(LocalDate tanggalPost) {
        this.tanggalPost = tanggalPost;
    }

    @Override
    public String toString() {
        return "Broadcast{" +
                "id=" + id +
                ", informasi='" + informasi + '\'' +
                ", link='" + link + '\'' +
                ", user=" + user +
                '}';
    }
}
