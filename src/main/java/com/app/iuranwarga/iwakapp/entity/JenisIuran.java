package com.app.iuranwarga.iwakapp.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "jenis_iuran")
public class JenisIuran {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nama_iuran")
    private String namaIuran;
    @Column(name = "nominal")
    private int nominal;
    @ManyToOne(fetch = FetchType.LAZY,cascade= {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToMany(fetch = FetchType.LAZY,cascade= {CascadeType.PERSIST, CascadeType.MERGE,
            CascadeType.DETACH, CascadeType.REFRESH})
    @JoinTable(name = "jenis_iuran_perbulan",
            joinColumns = @JoinColumn(name = "id_jenis_iuran"),
            inverseJoinColumns = @JoinColumn(name = "id_iuran_perbulan"))
    private Set<IuranPerbulan> iuranPerbulans = new HashSet<>();

    public JenisIuran() {
    }

    public JenisIuran(String namaIuran, int nominal, User user) {
        this.namaIuran = namaIuran;
        this.nominal = nominal;
        this.user = user;
    }

    public JenisIuran(String namaIuran, int nominal, User user, Set<IuranPerbulan> iuranPerbulans) {
        this.namaIuran = namaIuran;
        this.nominal = nominal;
        this.user = user;
        this.iuranPerbulans = iuranPerbulans;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamaIuran() {
        return namaIuran;
    }

    public void setNamaIuran(String namaIuran) {
        this.namaIuran = namaIuran;
    }

    public int getNominal() {
        return nominal;
    }

    public void setNominal(int nominal) {
        this.nominal = nominal;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<IuranPerbulan> getIuranPerbulans() {
        return iuranPerbulans;
    }

    public void setIuranPerbulans(Set<IuranPerbulan> iuranPerbulans) {
        this.iuranPerbulans = iuranPerbulans;
    }

    @Override
    public String toString() {
        return "JenisIuran{" +
                "namaIuran='" + namaIuran + '\'' +
                ", nominal=" + nominal +
                '}';
    }
}
