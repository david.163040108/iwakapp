package com.app.iuranwarga.iwakapp.dao;


import com.app.iuranwarga.iwakapp.entity.Role;

public interface RoleDao {

	public Role findRoleByName(String theRoleName);
	
}
