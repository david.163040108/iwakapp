package com.app.iuranwarga.iwakapp.dao;

import com.app.iuranwarga.iwakapp.entity.Broadcast;

import java.util.List;

public interface BroadcastDao {
    List<Broadcast> broadcastListByRt(String rt);
}
