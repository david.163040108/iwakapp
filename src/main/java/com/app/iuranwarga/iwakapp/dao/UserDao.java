package com.app.iuranwarga.iwakapp.dao;


import com.app.iuranwarga.iwakapp.entity.User;

public interface UserDao {

    public User findByUserName(String userName);
    
    public void save(User user);
    
}
