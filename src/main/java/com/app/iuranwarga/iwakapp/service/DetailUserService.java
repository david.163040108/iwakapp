package com.app.iuranwarga.iwakapp.service;

import com.app.iuranwarga.iwakapp.entity.DetailUser;
import com.app.iuranwarga.iwakapp.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface DetailUserService {

    Page<DetailUser> detilUserByRT(String rt, Pageable pageable);
    public void save(DetailUser detailUser);
    public void delete(DetailUser detailUser);
    public DetailUser findById(long id);
    public DetailUser findByUser(User user);
    public List<DetailUser> findUserByRT(String rt, boolean active);
    Page<DetailUser> findAllByuserRt(User userRt,Pageable pageable);
}
