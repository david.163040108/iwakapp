package com.app.iuranwarga.iwakapp.service.impl;

import com.app.iuranwarga.iwakapp.entity.Broadcast;
import com.app.iuranwarga.iwakapp.entity.IuranPerbulan;
import com.app.iuranwarga.iwakapp.repo.IuranPerbulanRepoJpa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Transactional
@Service
public class IuranPerbulanService {
    @Autowired
    private IuranPerbulanRepoJpa repoJpa;


    public IuranPerbulan findById(long id){
        Optional<IuranPerbulan> byId = repoJpa.findById(id);
        IuranPerbulan perbulan = null;
        if (byId.isPresent()) {
            perbulan = byId.get();
        }
        else {
            // we didn't find the employee
            throw new RuntimeException("Did not find employee id - " + id);
        }
        return perbulan;
    }
}
