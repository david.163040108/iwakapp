package com.app.iuranwarga.iwakapp.service.impl;

import com.app.iuranwarga.iwakapp.entity.TransaksiIuran;
import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.repo.TransaksiIuranRepo;
import com.app.iuranwarga.iwakapp.service.TransaksiIuranService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class TransakaksiIuranServiceImpl implements TransaksiIuranService {

    @Autowired
    private TransaksiIuranRepo transaksiIuranRepo;

    @Transactional
    @Override
    public Page<TransaksiIuran> transaksiByRt(String rt, Pageable pageable) {
        return transaksiIuranRepo.findAllByidRt(rt,pageable);
    }

    @Transactional
    @Override
    public void save(TransaksiIuran transaksiIuran) {
        transaksiIuranRepo.save(transaksiIuran);
    }
    @Transactional
    @Override
    public Page<TransaksiIuran> findByUser(User user, Pageable pageable) {
        return transaksiIuranRepo.findAllByUser(user,pageable);
    }

    @Transactional
    @Override
    public Page<TransaksiIuran> findAllByUserAndActive(User user, boolean active, Pageable pageable) {
        return transaksiIuranRepo.findAllByUserAndActive(user,active,pageable);
    }
}
