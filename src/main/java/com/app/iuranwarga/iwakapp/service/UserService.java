package com.app.iuranwarga.iwakapp.service;

import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.user.CrmUser;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    public User findByUserName(String userName);

    public void save(CrmUser crmUser);
}
