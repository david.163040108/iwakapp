package com.app.iuranwarga.iwakapp.service;

import com.app.iuranwarga.iwakapp.entity.TransaksiIuran;
import com.app.iuranwarga.iwakapp.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TransaksiIuranService {
    Page<TransaksiIuran> transaksiByRt(String rt, Pageable pageable);

    public void save (TransaksiIuran transaksiIuran);
    Page<TransaksiIuran> findByUser(User user ,Pageable pageable);
    Page<TransaksiIuran> findAllByUserAndActive(User user,boolean active ,Pageable pageable);
}
