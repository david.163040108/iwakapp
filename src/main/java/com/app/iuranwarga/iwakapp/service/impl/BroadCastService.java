package com.app.iuranwarga.iwakapp.service.impl;

import com.app.iuranwarga.iwakapp.entity.Broadcast;
import com.app.iuranwarga.iwakapp.entity.JenisIuran;
import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.repo.BroadCastRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BroadCastService {

    @Autowired
    private BroadCastRepo broadCastRepo;

    public List<Broadcast> findAll(){
        return broadCastRepo.findAll();
    }

    public void save(Broadcast broadcast){
        broadCastRepo.save(broadcast);
    }

    public void delete(long id){
        broadCastRepo.deleteById(id);
    }

    public Broadcast findById(long id){
        Optional<Broadcast> byId = broadCastRepo.findById(id);
        Broadcast broadcast = null;
        if (byId.isPresent()) {
            broadcast = byId.get();
        }
        else {
            // we didn't find the employee
            throw new RuntimeException("Did not find employee id - " + id);
        }
        return broadcast;
    }

    public Page<Broadcast> findByUser(User user, Pageable pageable){
        return  broadCastRepo.findAllByUser(user, pageable);
    }
}
