package com.app.iuranwarga.iwakapp.service.impl;

import com.app.iuranwarga.iwakapp.entity.JenisIuran;
import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.repo.JenisIuranRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class JenisIuranService {
    @Autowired
    private JenisIuranRepo jenisIuranRepo;

    public List<JenisIuran> getByRt(User user){
        return  jenisIuranRepo.findByUser(user);
    }

    public void save(JenisIuran jenisIuran){
        jenisIuranRepo.save(jenisIuran);
    }

    public void delete(int id){
        Optional<JenisIuran> jsiuran = jenisIuranRepo.findById(id);
        JenisIuran jenisIuran = null;
        if (jsiuran.isPresent()) {
            jenisIuran = jsiuran.get();
            jenisIuranRepo.delete(jenisIuran);
        }
        else {
            // we didn't find the employee
            throw new RuntimeException("Did not find employee id - " + id);
        }
    }

    public JenisIuran findById(int id){
        Optional<JenisIuran> jsiuran = jenisIuranRepo.findById(id);
        JenisIuran jenisIuran = null;
        if (jsiuran.isPresent()) {
            jenisIuran = jsiuran.get();
        }
        else {
            // we didn't find the employee
            throw new RuntimeException("Did not find employee id - " + id);
        }
        return jenisIuran;
    }
}
