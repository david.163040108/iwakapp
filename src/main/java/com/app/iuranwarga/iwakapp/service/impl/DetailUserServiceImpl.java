package com.app.iuranwarga.iwakapp.service.impl;

import com.app.iuranwarga.iwakapp.entity.DetailUser;
import com.app.iuranwarga.iwakapp.entity.User;
import com.app.iuranwarga.iwakapp.repo.DetailUserRepo;
import com.app.iuranwarga.iwakapp.service.DetailUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DetailUserServiceImpl implements DetailUserService {

    @Autowired
    private DetailUserRepo detailUserRepo;


    @Override
    public Page<DetailUser> detilUserByRT(String rt, Pageable pageable) {
        return detailUserRepo.findAllByidRt(rt, pageable);
    }

    @Override
    public void save(DetailUser detailUser) {

        detailUserRepo.save(detailUser);
    }

    @Override
    public void delete(DetailUser detailUser) {

    }

    @Override
    public DetailUser findById(long id) {
        Optional<DetailUser> byId = detailUserRepo.findById(id);
        DetailUser detailUser = null;
        if (byId.isPresent()) {
            detailUser = byId.get();
        } else {
            // we didn't find the employee
            throw new RuntimeException("Did not find  id - " + id);
        }
        return detailUser;
    }

    @Override
    public DetailUser findByUser(User user) {
        return detailUserRepo.findByUser(user);
    }

    @Override
    public List<DetailUser> findUserByRT(String rt, boolean active) {
        return null;
    }

    @Override
    public Page<DetailUser> findAllByuserRt(User userRt, Pageable pageable) {
        return detailUserRepo.findAllByuserRt(userRt,pageable);
    }
}
